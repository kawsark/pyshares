from utils import isFloat
import iex
import os
import bcolors as bc
import json
from datetime import datetime

SELLTHRESHOLD = os.getenv("SELLTHRESHOLD",20.0)
PERIOD = os.getenv("PERIOD",90)
STOPLOSS = os.getenv("STOPLOSS",20.0)
DELTA52HIGH = os.getenv("DELTA52HIGH",5.0)
BUYREC = os.getenv("BUYREC",'[ {"day5ChangePercent":"-"}, {"month1ChangePercent":"+"}, {"month3ChangePercent":"+"}, {"day50MovingAvg":"-"}, {"year1ChangePercent":"+"} ]')
DATEFORMAT = "%y.%m.%d-%H:%M"
DEBUG = bool(os.getenv("DEBUG",None))

class Share:
    """ Share class represents an equity """

    name = None
    age = ""
    age_int = 0
    rsi_14 = -1.0
    shares = 0.0
    price = 0.0
    stats = None
    total_value = 0.0
    is_active = False
    category = ""
    primary_category = ""
    buy_recommendation = False
    sell_recommendation = False
    earnings_date = None
    earnings_date_distance = None
    pre_earnings = False

    total_return_str = None
    return_amount = 0.0

    #TODO: Implement return percent as an array
    return_percent = 0.0
    highest_return_percent = 0.0
    lowest_return_percent = 0.0

    original_invested = 0.0
    original_invested_per_share = 0.0
    portfolio_percentage = 0.0
    avg_cost_basis = 0.0

    def __init__(self,name):
        self.name = name
    
    def get_is_active(self):
        return self.is_active

    '''
    Sets the number of shares in this security and turns it to Active
    '''
    def set_shares(self,shares):
        self.shares = shares
        if self.shares > 0:
            self.is_active = True

    '''
    Set the average cost basis per share for this security
    '''
    def set_avg_cost_basis(self,avg_cost_basis):
        if isFloat(avg_cost_basis):
            self.avg_cost_basis = float(avg_cost_basis)
            self.original_invested = (self.shares * self.avg_cost_basis)
            # Hack for stocks obtained as free
            if self.original_invested == 0:
                self.original_invested = 1.0
            self.total_value = self.price * self.shares
            self.return_percent = (self.total_value / self.original_invested) * 100

            # Adjust high and low
            if self.return_percent > self.highest_return_percent:
                self.highest_return_percent = self.return_percent

            if self.lowest_return_percent == 0.0 or self.return_percent < self.lowest_return_percent:
                self.lowest_return_percent = self.return_percent
    
            #self.avg_cost_basis = self.original_invested / self.shares
            self.set_sell_recommendations()

        else:
            print("WARN: was unable to quantify cost basis string to float: %s" % avg_cost_basis)
    
    def get_avg_cost_basis(self):
        if self.is_active:
            return self.avg_cost_basis
        else:
            return -1

    '''
    Set the total gain or loss of a position as a String such as -$xx.yy
    '''
    def set_total_return_str(self,total_return_str):
        self.total_return_str = total_return_str        
        error = False
        s = self.total_return_str.split('$')
        if len(s) >= 2:
            s1 = ''.join((s[0],s[1]))
            if isFloat(s1):
                self.return_amount = float(s1)
                self.original_invested = (self.total_value - self.return_amount)
                # Hack for stocks obtained as free
                if self.original_invested == 0:
                    self.original_invested = 1.0
                self.total_value = self.price * self.shares
                self.return_percent = (self.total_value / self.original_invested) * 100
                self.avg_cost_basis = self.original_invested / self.shares
                self.set_sell_recommendations()

            else:
                error = True
        else:
            error = True

        if error:    
            print("WARN: was unable to quantify return string: %s. Expected format: +$5.0. -$1 etc." % self.total_return_str)

    def set_primary_category(self,primary_category):
        self.primary_category = primary_category

    def update_portfolio_percentage(self,portfolio_total):
        self.portfolio_percentage = self.total_value / portfolio_total * 100

    def set_earnings_date(self):
        earnings_date_str = iex.get_upcoming_events(self.name)

        if earnings_date_str is None or len(earnings_date_str) == 0:
            if self.stats and "nextEarningsDate" in self.stats:
                earnings_date_str = self.stats["nextEarningsDate"]

        if earnings_date_str and len(earnings_date_str) > 0:
            date_time_obj = datetime.strptime(earnings_date_str, '%Y-%m-%d')
            self.earnings_date = date_time_obj
            self.earnings_date_distance = (datetime.now() - date_time_obj).days
            if self.earnings_date_distance >= -14 and self.earnings_date_distance <= 0:
                self.pre_earnings = True

    '''
    cat_name = the category name as part of which we will print out the stats
    '''
    def get_stats_str(self, cat_name):
        if self.is_active:
            active = ' [Active]'
        else:
            active = ''
        
        delta_percent_52 = self.get_52_week_high_delta()
        result_str = "{cat_name},{name}{is_active}, ${price}, {primary_category}, {category}, {delta_percent_52:.2f}%"
        result = result_str.format(cat_name=cat_name, name=self.name,is_active=active,price=self.price,primary_category=self.primary_category,category=self.category,delta_percent_52=delta_percent_52)
        
        stats_str=["day5ChangePercent","month1ChangePercent","month3ChangePercent","month6ChangePercent","year1ChangePercent","year5ChangePercent"]
        stats_result = ''
        for stat in stats_str:
            try:
                stats_result = '{},{:.2f}%'.format(stats_result, self.stats[stat])
            except (KeyError, TypeError) as error:
                print("WARN: Encountered error when formatting stats for symbol %s: %s" % (self.name,error))
                stats_result = '{},0.0%'.format(stats_result)
            
        result = ''.join((result,stats_result,",",str(self.buy_recommendation)))
        
        if self.buy_recommendation:
            result = ''.join((bc.OKGREEN,result,bc.ENDC))
        else:
            result = ''.join((bc.ENDC,result,bc.ENDC))
        
        return result
    
    def get_52_week_high_delta(self):
        result = 0
        week52high_str = self.stats["week52high"]
        if isFloat(week52high_str):
            week52high = float(week52high_str)
            if week52high > 0:
                delta = week52high - self.price
                result = (delta / week52high) * 100
            else:
                print("WARN: Encountered Zero error when converting 52 Weeks High delta for symbol %s. week52high is: %s" % (self.name,week52high_str))
        
        return result


    '''
        Downloads the latest Price, Category and Statistics from API
        Must be called before calculating value or sell recommendations
    '''
    def refresh(self):
        # Set Category
        self.category = iex.get_company_info(self.name,field="sector")
        if self.primary_category == "Unassigned":
            self.primary_category = self.category

        # Set the latest price
        latest_price = iex.get_price(self.name)
        if latest_price and isFloat(latest_price):
            self.price = float(latest_price)

            if self.is_active and isFloat(self.avg_cost_basis):
                self.total_value = self.price * self.shares
                self.return_percent = (self.total_value / self.original_invested) * 100
                
                # Adjust high and low percentages
                if self.return_percent > self.highest_return_percent:
                    self.highest_return_percent = self.return_percent
                
                if self.return_percent < self.lowest_return_percent:
                    self.lowest_return_percent = self.return_percent
                
                # Persist updated values
                self.refresh_metadata()

        else:
            print("WARN: Could not get price for symbol: %s" % self.name)

        # Set the latest stats
        latest_stats = iex.get_stats(self.name)
        if latest_stats:
            self.stats = latest_stats
        else:
            print("WARN: Could not get stats for symbol: %s" % self.name)
        
        # Set the RSI
        self.rsi_14 = float(iex.get_rsi(self.name))
        self.stats["rsi"] = self.rsi_14

        # Set the earnings date
        self.set_earnings_date()
        
        # Set recommendations
        # self.set_buy_recommendations()
            
    def set_buy_recommendations(self):
        # Set buy recommendation

        # First check that the 52 week delta is at least what was specified
        result = self.get_52_week_high_delta() >= float(DELTA52HIGH)

        # Load the Buy recommendation string
        buyrec = json.loads(BUYREC)
        for b in buyrec:
            if not result:
                self.buy_recommendation = result
                return result

            # Examine each item in Buy recommendation string
            k,v = b.popitem()
            try:
                if "ChangePercent" in k:
                    if v == "+":
                        result = result and (float(self.stats[k]) > 0)
                    else:
                        result = result and (float(self.stats[k]) <= 0)
                elif "MovingAvg" in k:
                    if v == "+":
                        result = result and (self.price > float(self.stats[k]))
                    else:
                        result = result and (self.price <= float(self.stats[k]))
            except TypeError as te:
                # There may be a situation where the stats returned from the API is null. 
                print("WARN: Encountered error when processing buy recommendation for symbol %s: %s" % (self.name,te))
                result = False
                
            else:
                pass

        self.buy_recommendation = result
        return result
    
    def set_sell_recommendations(self):
        if not self.is_active:
            self.sell_recommendation = False
        else:
            # Refresh metadata associated with age etc.
            self.refresh_metadata()

            self.sell_recommendation = self.age_int > PERIOD
            if DEBUG and self.sell_recommendation:
                print("DEBUG: Sell recommendation of %s set to %s due to age: %d" % (self.name,str(self.sell_recommendation),self.age_int))

            if not self.sell_recommendation:
                max = (100 + float(SELLTHRESHOLD))
                min = (100 - float(STOPLOSS))
                self.sell_recommendation = (self.return_percent >= max) or (self.return_percent <= min)
                if DEBUG and self.sell_recommendation:
                    if (self.return_percent >= max):
                        print("DEBUG: Sell recommendation of %s set to %s due to range:  <%s----%s> | %%" % (self.name,str(self.sell_recommendation),str(min),str(max)))
                    else:
                        print("DEBUG: Sell recommendation of %s set to %s due to range: | <%s----%s> %%" % (self.name,str(self.sell_recommendation),str(min),str(max)))

    def get_total_value(self):
        return self.total_value

    def get_original_invested(self):
        return self.original_invested
    
    def get_return_percent(self):
        return self.return_percent
    
    def get_return_amount(self):
        return self.return_amount
    
    def to_string(self):
        if self.is_active:
            active = ' [Active]'
        else:
            active = ''

        if self.sell_recommendation:
            result = "Portfolio, {name}{is_active}, {primary_category}, {category}, {shares}, ${price:.2f}, ${original_invested:.2f}, ${total_value:.2f}, {portfolio_percentage:.2f}%, ${return_amount:.2f}, {highlight1}{return_percent:.2f}%, {age}, {rec}".format(name = self.name, is_active = active, primary_category = self.primary_category, category = self.category, shares = self.shares, price = self.price, original_invested = self.original_invested, total_value = self.total_value, portfolio_percentage = self.portfolio_percentage, return_amount = self.return_amount, return_percent = self.return_percent, highlight1 = bc.BOLD, age = self.age, rec = "Sell")
            result = ''.join((bc.WARNING,result,bc.ENDC))
        elif self.buy_recommendation:
            result = "Portfolio, {name}{is_active}, {primary_category}, {category}, {shares}, ${price:.2f}, ${original_invested:.2f}, ${total_value:.2f}, {portfolio_percentage:.2f}%, ${return_amount:.2f}, {highlight1}{return_percent:.2f}%, {age}, {rec}".format(name = self.name, is_active = active, primary_category = self.primary_category, category = self.category, shares = self.shares, price = self.price, original_invested = self.original_invested, total_value = self.total_value, portfolio_percentage = self.portfolio_percentage, return_amount = self.return_amount, return_percent = self.return_percent, highlight1 = bc.BOLD, age = self.age, rec = "Buy")
            result = ''.join((bc.OKGREEN,result,bc.ENDC))
        else:
            result = "Portfolio, {name}{is_active}, {primary_category}, {category}, {shares}, ${price:.2f}, ${original_invested:.2f}, ${total_value:.2f}, {portfolio_percentage:.2f}%, ${return_amount:.2f}, {return_percent:.2f}%, {age}, {rec}".format(name = self.name, is_active = active, primary_category = self.primary_category, category = self.category, shares = self.shares, price = self.price, original_invested = self.original_invested, total_value = self.total_value, portfolio_percentage = self.portfolio_percentage, return_amount = self.return_amount, return_percent = self.return_percent, age = self.age, rec = "Hold")

        return result
    
    """ Reads a metadata file (if it exists)
        Persist the last_active and earliest_active timestamps
        Also sets the age property
        TODO: Persist additional metadata such as value and return
    """
    def refresh_metadata(self):
        delta = 0
        result = None
        filename = 'data/{}-meta.json'.format(self.name) 

        # Read values into result
        if os.path.isfile(filename):
            with open(filename) as f:
                result = json.load(f)

                # check the previously highest return amount
                if "highest_return" in result and isFloat(result["highest_return"]):
                    highest_return_temp = float(result["highest_return"])
                    if self.highest_return_percent < highest_return_temp:
                        self.highest_return_percent = highest_return_temp

                # check the previously lowest return amount
                if "lowest_return" in result and isFloat(result["lowest_return"]):
                    lowest_return_temp = float(result["lowest_return"])
                    if lowest_return_temp != 0.0 and self.lowest_return_percent > lowest_return_temp:
                        self.lowest_return_percent = lowest_return_temp
                

        if not result:
            result = dict()

        # Update result and write to file
        if self.is_active:
            t = datetime.now()
            ts = t.strftime(DATEFORMAT)
            result["last_active"] = ts
            result["highest_return"] = str(self.highest_return_percent)
            result["lowest_return"] = str(self.lowest_return_percent)
            result["return_percent"] = str(self.return_percent)
            result["return_amount"] = str(self.total_value-self.original_invested)

            # Write the earliest active timestamp
            if "earliest_active" in result and len(result["earliest_active"]) > 0:
                t1 = datetime.strptime(result["earliest_active"], DATEFORMAT)
                delta = abs(t-t1).days
                self.age_int = delta
                self.age = '{} days'.format(delta)

            else: # No previous records exist, so write a new one
                result["earliest_active"] = ts
                self.earliest_active = t
                self.age_int = delta
                self.age = '0 days'

            result["age"] = self.age

            # Write updated file
            with open(filename,"w") as f:
                json.dump(result,f)
        
        return result

# Returns a header for CSV format data that aligns with to_string
def get_portfolio_header_string(cat_name):
    return ''.join((bc.BOLD,"{cat_name}, Name, Primary, Sector, Shares, Price, Invested, Value, Portfolio Percentage, Return, Percentage, Rec, Age".format(cat_name=cat_name), bc.ENDC))

# Returns a header for CSV format data that aligns with process_stats
def get_stats_header(cat_name):
    return ''.join((bc.BOLD,"{cat_name}, Name, Price, Primary, Sector, 52 weeks Delta, Change: 5 Day, 1 Month, 3 Months, 6 Months, 1 Year, 5 Years, Buy?".format(cat_name=cat_name), bc.ENDC))