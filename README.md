### API Calls
https://iexcloud.io/docs/api/#key-stats
https://iexcloud.io/docs/api/#company


### Usage
```bash
python3 pyshares.py
```

#### Printing stats
```bash
export PRINTSTATS=True
```

#### Buy Recommendations
```bash
# Most momentum: 5 day, 30 day, 50 day moving average, 90 days, 365 days 
$ export BUYREC='[ {"day5ChangePercent":"-"}, {"month1ChangePercent":"+"}, {"day50MovingAvg":"+"}, {"month3ChangePercent":"+"},  {"day200MovingAvg":"+"}, {"year1ChangePercent":"+"} ]'

# Default: 5 day, 30 day, 50 day moving average, 90 days, 365 days 
$ export BUYREC='[ {"day5ChangePercent":"-"}, {"month1ChangePercent":"+"}, {"day50MovingAvg":"-"}, {"month3ChangePercent":"+"}, {"year1ChangePercent":"+"} ]'

# Slightly longer but trending 
$ export BUYREC='[ {"day5ChangePercent":"-"}, {"month1ChangePercent":"-"}, {"day50MovingAvg":"+"},{"day200MovingAvg":"+"},  {"month3ChangePercent":"+"}, {"month6ChangePercent":"+"}, {"year1ChangePercent":"+"} ]'

# Pure momentum, no dips
$ export BUYREC='[ {"day50MovingAvg":"+"}, {"day200MovingAvg":"+"}, {"month3ChangePercent":"+"}, {"month6ChangePercent":"+"}, {"year1ChangePercent":"+"} ]'

# Moving average 50 and 200 days
# $ export BUYREC='[ {"day50MovingAvg":"-"}, {"month3ChangePercent":"+"}, {"month6ChangePercent":"+"}, {"year1ChangePercent":"+"} ]'
```

## TODO
- Create a new separate portfolio dict for active shares (for the purposes for sorting)
- Add sorting to Portfolio dict
- Key functionality
  - View allocation by arbitrary defined lists
  - Define simple buy and sell rules
  - Allow continuous scanning of portfolio and watchlists