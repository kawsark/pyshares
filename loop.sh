#!/bin/bash
[[ -z $INTERVAL ]] && INTERVAL=1800

for i in {1..20}
do 
  python3 pyshares.py
  sleep $INTERVAL
done
