import share
import json
import category
import bcolors as bc
import os

DEBUG = bool(os.getenv("DEBUG",None))

class Group:
    """ Group class represents a set of categories """

    name = None
    categories_dict = None

    def __init__(self,name,categories_dict):
        self.name = name
        self.categories_dict = categories_dict

    # Return the category dict
    def get_categories(self):
        return self.categories_dict

    # Get total invested across all categories in this group
    def get_group_invested(self):
        total_invested = 0.0
        x = 0
        for c in self.categories_dict.values():
            x=x+1
            #c.remove_inactive()
            total_invested = total_invested + c.get_total_invested()
        
        if DEBUG:
            print("DEBUG: Totalled %d Categories when calculating invested" % x)
        return total_invested
    

    # returns a dict with all symbols
    def get_all_symbols(self):
        result = set()
        for c in self.categories_dict.values():
            for symbol in c.category_shares.keys():
                result.add(symbol)
        
        return result

    # Get total market value across all categories in this group
    def get_group_value(self):
        total_value = 0.0
        x = 0
        for c in self.categories_dict.values():
            x=x+1
            #c.remove_inactive()
            total_value = total_value + c.get_total_value()
        
        if DEBUG:
            print("DEBUG: Totalled %d Categories when calculating values" % x)
        return total_value

    # Print Group
    def printGroup(self, portfolio_invested=0):    
        total_invested = self.get_group_invested()
        total_value = self.get_group_value()
        return_percent = 0
        portfolio_percentage = 0
        if total_invested != 0:
            return_percent = total_value/total_invested * 100
            portfolio_percentage = total_invested/portfolio_invested * 100

        categories = self.categories_dict
        print("%s%s%s: Total invested: %.2f [%.2f %%], return: %.2f [%.2f %%] across %d categories%s" % (bc.BOLD, bc.OKBLUE, self.name, total_invested, portfolio_percentage, total_value, return_percent, len(categories), bc.ENDC))
        print(category.get_header_string(self.name))

        # If a portfolio invested amount was not set, then use this groups total
        if portfolio_invested == 0:
            portfolio_invested = total_invested

        # Print each category in this group
        for c in categories.values():
            # Get % of Total investment in this category
            invested = c.get_total_invested()
            p = (invested / portfolio_invested * 100)
            print("{}, {:.2f}%".format(c.to_string(self.name), p))
            print(c.get_shares_string())
