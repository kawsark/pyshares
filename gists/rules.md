### rules

- Each equity will be no greater than 4% to 5% of total funds. This means at least 20 to 25 Equities are needed for full Investment. If there are fewer than 20 Equities, then the remaining will be cash on hand.
- Each equity will have a default holding period associated with it, for example 3 months. 
- Each equity will be part of a primary category. There should be 4 to 5 categories with as much even distribution as possible. No single category should exceed 40% of total funds.