import requests
import os
import json
import time

# IEX parameters
api_key=os.getenv("IEX_KEY")
base_url="https://cloud.iexapis.com/stable"
company_endpoint="/stock/{}/company"
rsi_endpoint="/stock/{}/indicator/rsi?range=2m&input1={}&lastIndicator=true&indicatorOnly=true"
price_endpoint="/stock/{}/price"
earnings_endpoint="/stock/{}/upcoming-events"
stats_endpoint="/stock/{}/stats"
token="?token={}"
and_token="&token={}"
DEBUG = bool(os.getenv("NET_DEBUG",None))

'''
    Reaches out to the IEX API at the indicated endpoint
    It handles retries and errors
'''
def get_response(endpoint_url, retry=0):
    result = None

    if api_key:
        final_url = '{}{}'.format(base_url,endpoint_url)
        if DEBUG:
            print("DEBUG: Request ---> ", final_url)

        if "?" in final_url:
            final_url = '{}{}'.format(final_url, and_token.format(api_key))
        else:
            final_url = '{}{}'.format(final_url, token.format(api_key))

        try:
            response = requests.get(final_url)
            if DEBUG:
                print("DEBUG: Response <--- ", response)

            if response.status_code == 200:
                result = response
            else:
                print("ERROR: Received non 200 return code: %d, %s" % (response.status_code, response.text))

        except(requests.exceptions.RequestException) as err:
            print("ERROR: received connection error for url %s%s: %s" %(base_url,endpoint_url,err))

            if retry < 3:
                print("INFO: Pausing to retry [%d/%d] " % (retry,3))
                time.sleep(5)
                rt = retry + 1
                return get_response(endpoint_url,rt)
            else:
                print("ERROR: Giving up after 3 tries")

    else:
        print("ERROR: IEX_KEY environment variable is not set.")


    return result


'''
    Obtains the RSI
'''
def get_rsi(symbol, period=14, recurse=0):
    result = None
    url = rsi_endpoint.format(symbol,period)
    resp = get_response(url)
    if resp:
        result = resp.json()["indicator"][0][0]
        if DEBUG:
            print("DEBUG: RSI for: ", symbol, result)
    else:
        print("ERROR: Unable to obtain RSI from API (see earlier errors)")
    
    return result

'''
    Obtains Upcoming Egents
'''
def get_upcoming_events(symbol,event="earnings",recurse=0):
    result = None
    url = earnings_endpoint.format(symbol)
    resp = get_response(url)
    error = False
    if resp:
        events = resp.json()
        earnings = events[event]
        if DEBUG:
            print("DEBUG: Parsing events: ", events)
        
        if len(earnings) > 0:
            result = earnings[0]["reportDate"]
        else:
            print("WARN: earnings event was empty for ", symbol)
            error = True
    else:
        error = True

    if error:
        print("ERROR: Unable to obtain Upcoming Events from API (see earlier errors)")
    
    return result

'''
    Obtains the latest price
'''
def get_price(symbol,recurse=0):
    result = None
    url = price_endpoint.format(symbol)
    resp = get_response(url)
    if resp:
        result = resp.text
        if DEBUG:
            print("DEBUG: Price for: ", symbol, result)
    else:
        print("ERROR: Unable to obtain Price from API (see earlier errors)")
    
    return result

'''
    Obtain the company data and write it to file
'''
def download_company_data(symbol,recurse=0):
    result = None
    url = company_endpoint.format(symbol)
    resp = get_response(url)
    if resp:
        result = resp.json()
        with open('data/{}.json'.format(symbol),"w") as f:
            f.write(resp.text)
            f.close()

        if DEBUG:
            print("DEBUG: Company info for: ", symbol, result)
    else:
        print("ERROR: Unable to company info from API (see earlier errors)")
    
    return result

'''
    Obtain the latest Stats
'''
def get_stats(symbol,recurse=0):
    result = None
    url = stats_endpoint.format(symbol)
    resp = get_response(url)
    if resp:
        result = resp.json()
        with open('data/{}-stats.json'.format(symbol),"w") as f:
            f.write(resp.text)
            f.close()

        if DEBUG:
            print("DEBUG: Company info for: ", symbol, result)
    else:
        print("ERROR: Unable to company info from API (see earlier errors)")
    
    return result

'''
    Gets Company information if it does not exist already
'''
def get_company_info(symbol, update=False, field = None):
    j = None
    filename = 'data/{}.json'.format(symbol)
    if update or not os.path.isfile(filename):
        dresult = download_company_data(symbol)
        if not dresult:
            print("ERROR: could not obtain information for symbol: %s " % symbol)
            return None

    with open(filename,'r') as f:
        j = f.read()
        js = json.loads(j)
    
    result = None

    if j and js:
        # Return a specific field
        if field:
            if field in js:
                result = js[field]
            else:
                print("ERROR: requested field %s not available in Company Data." % field)
        # Return the entire response
        else:
            result = j

    else:
        print("ERROR: could not load data, please see earlier errors.")
    
    return result
