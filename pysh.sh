#!/bin/bash

[[ -z $IEX_KEY ]] && echo "Please set the IEX_KEY environment variable first" && exit

# Optionally set THRESHOLD and STOPLOSS
# export THRESHOLD=5
# export STOPLOSS=30
# export DELTA52HIGH=5

python3 pyshares.py > p.csv

# Write out Stats
cat p.csv | grep PYSH-ST > stats.csv

# Write out Shares
cat p.csv | grep PYSH-SH > shares.csv

# Write out Categories
cat p.csv | grep PYSH-CA > cat.csv

cat p.csv | grep True

# IPOs
if [[ $IPO == "True" ]]
then
  base_url="https://cloud.iexapis.com/stable"
  curl -s "${base_url}/time-series/PREMIUM_WALLSTREETHORIZON_INITIAL_PUBLIC_OFFERING?token=${IEX_KEY}" > ipo.json
  cat ipo.json| jq '.[] | .iposcoopCompanyName, .industry, .offeringdate, .symbol'
fi
