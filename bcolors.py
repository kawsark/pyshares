HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

# Render input as Green
def green(input,end=ENDC):
    return ''.join((OKGREEN,input,end))

# Render input as Yellow
def warning(input,end=ENDC):
    return ''.join((WARNING,input,end))

# Render input as Blue
def blue(input,end=ENDC):
    return ''.join((OKBLUE,input,end))

# Render input as Red
def red(input,end=ENDC):
    return ''.join((FAIL,input,end))

# Render input as Bold
def bold(input,end=ENDC):
    return ''.join((BOLD,input,end))
