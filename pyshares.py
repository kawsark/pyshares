import re
import time
import requests
import iex
import os
import json, glob, argparse
import share as sh
import category
import sys, traceback
import bcolors as bc
from utils import isFloat
from datetime import datetime

# Graphing
# https://matplotlib.org/3.3.2/api/index.html#the-pyplot-api
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

DATEFORMAT = "%y.%m.%d-%H:%M"
PORTFOLIO_TYPE = os.getenv("PORTFOLIO_TYPE")
PORTFOLIO_FILE = os.getenv("PORTFOLIO_FILE",'portfolio.html')
CATEGORY_FILE = os.getenv("CATEGORY_FILE",'strategy.json')
DEBUG = bool(os.getenv("DEBUG",None))

'''
    This function will clean up all metadata files that are in the data directory that are no longer active
'''
def cleanOldMetaFiles(portfolio):
    sold = 0
    metaFilenamesList = glob.glob('data/*-meta.json')
    for filename in metaFilenamesList:
        symbol = filename[5:filename.find('-')]
        if symbol not in portfolio:
            meta = None
            with open(filename) as f:
                meta = json.load(f)

            # If last_active exists then set it as last traded value
            if "last_active" in meta and len(meta["last_active"]) > 0:
                sold = sold + 1
                meta["last_traded"] = meta["last_active"]
                t = datetime.now()
                ts = t.strftime(DATEFORMAT)
                if "return_percent" in meta and "age" in meta:
                    return_percent = float(meta["return_percent"]) - 100
                    return_amount = float(meta["return_amount"])
                    comment = '{} - Closed {} near {} with returns: {:.2f}%, ${:.2f} after {}'.format(ts,symbol,meta["last_active"],return_percent, return_amount, meta["age"])
                else:
                    comment = '{} - Closed {} near {}'.format(ts,symbol,meta["last_active"])
                    print("ERROR: Could not find return_percent in file: ", filename)
                
                print("INFO: %s" % (comment))
                
                # Write to trades.log
                with open("data/trades.log", "a+") as f:
                    f.write(comment)
                    f.write("")

                # Reset values to null
                meta["last_active"] = ""
                meta["earliest_active"] = ""

                # Update the trade count
                trade_count = 1
                if "trade_count" in meta:
                    # increment the trade count
                    trade_count = int(meta["trade_count"]) + 1

                meta["trade_count"] = trade_count

                # Write the updated meta file
                with open(filename,"w") as f:
                    json.dump(meta,f)

                # Append and print comment

            else:
                pass

    return sold

# Read a file to get the Shares and total return
def parsePortfolioWebull(filename,allshares,categories):
    portfolio = dict()
    total_value = 0.0

    # Regex to parse Robinhood dashboard
    pattern1 = '<span>([A-Z]{1,5})</span>'
    pattern2 = '<span>[\s]*([0-9]+)</span>'
    pattern3 = '<span>[\s]*([0-9]+.[0-9]{3,3})</span>'

    html = None

    with open(filename,'r') as f:
        html = f.read()

    if html:
        p1 = re.compile(pattern1)
        m1 = re.finditer(pattern1, html)
        current_position = 0 
        for m in m1:
            symbol = m[1]
            if DEBUG:
                print("DEBUG: Parsing symbol: ", symbol)

            if m.start() < current_position:
                if DEBUG:
                    print("DEBUG: skipping symbol ", symbol, " due to position overlap")
                continue
            else:
                pass

            # Check if we have processed this symbol already
            if not symbol in portfolio.keys():
                shares = ""
                avg_price = ""
                p2 = re.compile(pattern2)
                m2 = p2.search(html,m.end())

                if m2: # Get qty. of Shares                        
                    shares = m2[1]
                    if DEBUG:
                        print("DEBUG: m2 matches: ", m2[0], m2[1])

                    p3 = re.compile(pattern3)

                    # Find the starting position for Avg price:

                    # First search for end of Market value column
                    pos = html.find('</span>', m2.end())

                    # Then search for end of Last price column
                    pos = html.find('</span>', pos+7)

                    # Now search for the Average price
                    m3 = p3.search(html,pos)
                    if m3:
                        if DEBUG:
                            print("DEBUG: m3 matches: ", m3[0], m3[1])

                        avg_price = m3[1]
                        current_position = m3.end()
                    else:
                        print("WARN: unable to find an average price for symbol: ", symbol)

                s = None
                if symbol in allshares:
                    # A Share object exists already
                    s = allshares[symbol]
                else:
                    # Create a new Share object and assign to Unassigned category
                    category_obj = categories["Unassigned"]
                    s = category.add_share_to_category(symbol,None,category_obj,True)
                    allshares[symbol] = s
                
                s.refresh()

                # Update Share with equity values and add to portfolio dict
                if isFloat(shares):
                    s.set_shares(float(shares))
                    s.set_avg_cost_basis(avg_price)
                    total_value = total_value + s.get_total_value()
                    portfolio[symbol] = s
                    print("%sINFO: Added %s shares of %s @ $%.3f per share%s" % (bc.BOLD, shares, symbol, s.get_avg_cost_basis(), bc.ENDC))
            else:
                # We came across this symbol before so skip it
                # print("Skipping existing: ", symbol)
                pass

        # Update each share with portfolio percentage
        for s in portfolio.values():
            s.update_portfolio_percentage(total_value)                    
        
        print("INFO: Total value: $%s" % total_value)
        return portfolio
    
    else:
        print("ERROR: Could not open %s" % filename)
        return None

# Read a file to get the Shares and total return
def parsePortfolioRH(filename,allshares,categories):
    portfolio = dict()
    total_value = 0.0

    # Regex to parse Robinhood dashboard
    pattern1 = 'href="/stocks/([A-Z]*)"'
    pattern2 = "([0-9\.]+) Share"
    pattern3 = '([\+-]\$[0-9\.]+)'

    html = None

    with open(filename,'r') as f:
        html = f.read()

    if html:
        p1 = re.compile(pattern1)
        m1 = re.finditer(pattern1, html)
        for m in m1:
            symbol = m[1]

            # Check if we have processed this symbol already
            if not symbol in portfolio.keys():
                shares = ""
                total_return = ""
                p2 = re.compile(pattern2)
                m2 = p2.search(html,m.end())

                if m2: # Get qty. of Shares
                    # Check if there is another symbol found next
                    nextp1 = p1.search(html,m.end())
                    if nextp1:
                        # Check that the # of Shares found is before the next Symbol starts
                        if m2.start() > nextp1.start():
                            if DEBUG:
                                print("DEBUG: m is: ", m, ", starting @", m.start())
                                print("DEBUG: m2 is: ", m2, ", starting @: ", m2.start())
                                print("DEBUG: next p1 is: ", nextp1, ", starting @: ", nextp1.start())
                            continue
                    else:
                        pass
                        
                    shares = m2[1]
                    p3 = re.compile(pattern3)
                    m3 = p3.search(html,m.start())
                    if m3: # Get Total return string
                        total_return = m3[0]

                s = None
                if symbol in allshares:
                    # A Share object exists already
                    s = allshares[symbol]
                else:
                    # Create a new Share object and assign to Unassigned category
                    category_obj = categories["Unassigned"]
                    s = category.add_share_to_category(symbol,None,category_obj,True)
                    allshares[symbol] = s
                
                s.refresh()

                # Update Share with equity values and add to portfolio dict
                if isFloat(shares):
                    s.set_shares(float(shares))
                    s.set_total_return_str(total_return)
                    total_value = total_value + s.get_total_value()
                    portfolio[symbol] = s
                    print("%sINFO: Added %s shares of %s @ $%.3f per share%s" % (bc.BOLD, shares, symbol, s.get_avg_cost_basis, bc.ENDC))
            else:
                # We came across this symbol before so skip it
                # print("Skipping existing: ", symbol)
                pass

        # Update each share with portfolio percentage
        for s in portfolio.values():
            s.update_portfolio_percentage(total_value)                    
        
        print("INFO: Total value: $%s" % total_value)
        return portfolio
    
    else:
        print("ERROR: Could not open %s" % filename)
        return None

def graphPortfolio(portfolio, active_only=True):
    symbols = list()
    rsi = list()
    age = list()
    return_percent = list()
    for share in portfolio.values():
        if share.get_is_active():
            symbols.append(share.name)
            rsi.append(share.rsi_14)
            age.append(share.age_int)
            return_percent.append(share.return_percent)

    # Figure Size 
    plt.figure(figsize = (10, 8)) 


    # RSI
    # creating the horizontal bar plot 
    plt.barh(symbols, rsi, color ='maroon') 

    # Labels
    plt.xlabel("Active symbols") 
    plt.ylabel('RSI')
    plt.title('Portfolio RSI 14 day')
    plt.axline((30, 0), (30, 25))
    plt.axline((70, 0), (70, 25))
    axes = plt.gca()
    axes.xaxis.grid()

    plt.show()

    # AGE
    plt.barh(symbols, age, color ='yellow') 
    plt.ylabel('Age')
    plt.title('Portfolio Age')
    plt.axline((90, 0), (90, 25), color = "red")
    axes = plt.gca()
    axes.xaxis.grid()
    plt.show()

    # RETURN
    plt.barh(symbols, return_percent, color ='green') 
    plt.ylabel('Return')
    plt.title('Portfolio Return Percent')
    plt.axline((100, 0), (100, 25), color = "blue")
    plt.axline((80, 0), (80, 25), color = "red")
    plt.axline((120, 0), (120, 25), color = "green")
    axes = plt.gca()
    axes.xaxis.grid()
    plt.show()


def printPortfolio(portfolio, hold_shares, active_only=True, print_details = True):
    print(sh.get_portfolio_header_string("Robinhood"))
    share_age_dict = dict()
    i = 0
    for share in portfolio.values():
        if share.get_is_active():
            share.refresh()
            if print_details:
                print("[%d] %s" % (i,share.to_string()))
            i = i + 1
            share_age_dict[share.name] = share.age_int
        else:
            if print_details:
                print(share.to_string())
    
    # Sort by age
    print("%sINFO: Sell by age: [" % bc.WARNING)
    sort_age = sorted(share_age_dict.items(), key=lambda x: x[1], reverse=True)
    for s in sort_age:
        share = portfolio[s[0]]

        # Store gain as a 2 percentage string
        gain = '{:.2f}%'.format(share.get_return_percent()-100)
        highest_gain = '{:.2f}%'.format(share.highest_return_percent-100)
        lowest_gain = '{:.2f}%'.format(share.lowest_return_percent-100)
        comment = ""

        # Highlight as Aged if over 90
        if share.age_int >= 90:
            comment = bc.warning("Aged",bc.WARNING)

        # Color code gain base on return percentage
        return_percent = share.get_return_percent()
        if  return_percent > 100:
            gain = ''.join((bc.BOLD,bc.green(gain),bc.ENDC,bc.WARNING))
            highest_gain = bc.green(highest_gain,bc.WARNING)
            lowest_gain = bc.green(lowest_gain,bc.WARNING)
    
            if return_percent < 103:
                if share.pre_earnings:
                    comment = ''.join((bc.BOLD,bc.green("ACTION ----> Hold if price action confidence"),bc.ENDC,bc.WARNING))
                else:
                    comment = ''.join((bc.BOLD,bc.green("Pre-action --> Hold for 5.5%"),bc.ENDC,bc.WARNING))
            elif return_percent < 105.5:
                comment = ''.join((bc.BOLD,bc.green("Pre-action --> Hold for 5.5%"),bc.ENDC,bc.WARNING))
            else:
                if not share.name in hold_shares:
                    comment = ''.join((bc.BOLD,bc.green("ACTION ----> Set Trailing stop variable"),bc.ENDC,bc.WARNING))
                else:
                    comment = bc.green("Hold long term",bc.WARNING)
        else:
            gain = ''.join((bc.BOLD,bc.red(gain),bc.ENDC,bc.WARNING))
            highest_gain = bc.red(highest_gain,bc.WARNING)
            lowest_gain = bc.red(lowest_gain,bc.WARNING)

            if return_percent > 95:
                if not share.name in hold_shares:
                    comment = ''.join((bc.BOLD,bc.red("ACTION ----> Trailing-stop 0.5%, or Hold for Price action confidence."),bc.ENDC,bc.WARNING))
                else:
                    comment = bc.red("Hold long term",bc.WARNING)
            else:
                if share.pre_earnings:
                    if not share.name in hold_shares:
                        comment = ''.join((bc.BOLD,bc.red("ACTION ----> Trailing-stop 0.5%, or Hold for Price action confidence."),bc.ENDC,bc.WARNING))
                    else:
                        comment = bc.red("Hold long term",bc.WARNING)
                else:
                    if not share.name in hold_shares:
                        comment = ''.join((bc.BOLD,bc.red("ACTION ----> Stoploss"),bc.ENDC,bc.WARNING))
                    else:
                        comment = bc.red("Hold long term",bc.WARNING)

        print("%s: %d days (%s <--low-- gain: %s --high--> %s): %s" % (s[0], s[1], lowest_gain, gain, highest_gain, comment))

        if share.earnings_date_distance:
            dist = share.earnings_date_distance
            date_str = share.earnings_date.strftime("%b %d")

            dist_str = ''.join((str(dist),"d"))
            if share.pre_earnings:
                dist_str = ''.join((bc.BOLD,str(dist),"d [Pre-earning]",bc.ENDC,bc.WARNING))
                
            print("--> Next earnings: %s (%s)" % (date_str, dist_str))
        else:
            print("--> Next earnings: -")

    print("]%s" % bc.ENDC)



# wget https://pkgstore.datahub.io/core/s-and-p-500-companies/constituents_json/data/8fd832966a715a70cb9cf3f723498e3b/constituents_json.json
# https://datahub.io/core/s-and-p-500-companies
if __name__ == "__main__":
    #print("INFO: Parsing S&P 500")
    #category.loadIndex('data/constituents_json.json')

    parser = argparse.ArgumentParser(description='Process portfolio from downloaded html file.')
    parser.add_argument("-g", "--graph", help="Provides a graph of portfolio position gains", action="store_true")
    parser.add_argument("-c", "--categories", help="Provides a breakdown of positions by categories", action="store_true")
    parser.add_argument("-l", "--loop", help="Loops every 5 minutes", action="store_true")
    parser.add_argument("-d", "--details", help="Show details for each position", action="store_true")
    args = parser.parse_args()


    print("INFO: Parsing primary categories")
    groups, allshares = category.loadCategories(CATEGORY_FILE)
    default_categories = groups["default"].get_categories()
    hold_shares = groups["hold"].get_all_symbols()
    print("INFO: Found %d symbols in long term hold" % len(hold_shares))
    
    print("INFO: Parsing Portfolio from: ", PORTFOLIO_FILE)
    if PORTFOLIO_TYPE == "Webull":
        portfolio = parsePortfolioWebull(PORTFOLIO_FILE,allshares,default_categories)
    elif PORTFOLIO_TYPE == "RobinHood":
        portfolio = parsePortfolioRH(PORTFOLIO_FILE,allshares,default_categories)
    else:
        print("ERROR: Please set PORTFOLIO_TYPE environment variable to Webull or Robinhood")
        exit(1)

    print("INFO: Loaded %d categories and %d symbols." % (len(default_categories),len(portfolio)))
    printPortfolio(portfolio, hold_shares, print_details=args.details)
    sold = cleanOldMetaFiles(portfolio)
    print("INFO: Closed %d previous positions" % sold)

    # Shows graphs with gain percentages
    if args.graph:
        graphPortfolio(portfolio)
    
    # Print all groups and categories
    if args.categories:
        portfolio_invested = groups["default"].get_group_invested()
        for group_name,group in groups.items():
            print("")
            group.printGroup(portfolio_invested)
    
    # Check for loop
    if args.loop:
        while True:
            print("INFO: Going to sleep for 300 seconds, Control-C to exit")
            time.sleep(300)
            printPortfolio(portfolio, hold_shares, print_details=args.details)

    