import share
import json
import category
import bcolors as bc
import os
import group

DEBUG = bool(os.getenv("DEBUG",None))
PRINTSTATS = bool(os.getenv("PRINTSTATS",None))

class Category:
    """ Category class represents an set of shares """

    price = 0.0
    total_value = 0.0
    is_primary = False
    category_shares = None

    def __init__(self,name,is_primary=False):
        self.name = name
        self.is_primary = is_primary
        self.category_shares = dict()

    def get_shares(self):
        return self.category_shares

    def get_is_primary(self):
        return self.is_primary

    def remove_inactive(self):
        keys = []
        for k,v in self.category_shares.items():
            if not v.get_is_active():
                keys.append(k)
                
        for k in keys:
            self.category_shares.pop(k)

    def get_num_of_symbols(self):
        return len(self.category_shares)
    
    def add_share(self,name, share):
        self.category_shares[name] = share

    def get_share(self,name):
        if name in self.category_shares:
            return self.category_shares[name]
        else:
            return None

    # Get the total market value of all shares in this category
    def get_total_value(self):
        result = 0.0
        x = 0
        for value in self.category_shares.values():
            if value.get_is_active():
                result = result + value.get_total_value()
                x=x+1
        
        if DEBUG:
            print("[%s] Added %d shares when calculating total value" % (self.name, x))

        return result
    
    # Get the total return amount of all shares in this category
    def get_total_return(self):
        result = 0.0
        for value in self.category_shares.values():
            if value.get_is_active():
                result = result + value.get_return_amount()
        
        return result

    # Get the total investment amount of all shares in this category
    def get_total_invested(self):
        result = 0.0
        x = 0
        for value in self.category_shares.values():
            if value.get_is_active():
                result = result + value.get_original_invested()
                x=x+1
        
        if DEBUG:
            print("[%s] Added %d shares when calculating total invested" % (self.name,x))

        return result
        
    def to_string(self, group_name=""):
        if self.is_primary:
            primary = ' [Primary]'
        else:
            primary = ''

        invested = self.get_total_invested()
        value = self.get_total_value()
        percentage_return = 0
        if invested > 0:
            percentage_return = (value/invested) * 100

        result = "{group_name}, {name}{is_primary}, {symbols}, ${total_value:.2f}, ${total_invested:.2f}, ${total_return:.2f}, {total_change:.2f} %".format(group_name=group_name, name = self.name, is_primary = primary, symbols = self.get_num_of_symbols(), total_value = value, total_invested = invested, total_return = self.get_total_return(), total_change = percentage_return)
        
        return result

    # Return a single string with all shares in this category
    def get_shares_string(self,active_only=True):
        result = []
        for symbol,v in self.category_shares.items():
            if active_only:
                if v.is_active:
                    result.append(symbol)
                else:
                    pass
            else:
                result.append(symbol)
        
        if active_only:
            result_str = "  --> Active (" + str(len(result)) + "): " + str(result)
        else:
            result_str = "  --> Shares (" + str(len(result)) + "): " + str(result)
        
        return result_str


    # Return a single string with all shares with a buy recommendation in this category
    def get_buys_string(self):
        result = []
        for symbol,v in self.category_shares.items():
            if v.buy_recommendation:
                result.append(symbol)
            else:
                pass
        
        result_str = "  --> Buys (" + str(len(result)) + "): " + str(result)
                
        return result_str

EXCLUDESECTORS = os.getenv("EXCLUDESECTORS",'["Financials"]')
BUYONLY = os.getenv("BUYONLY","False")

# Returns a header for CSV format data that aligns with to_string
def get_header_string(group_name):
    return ''.join((bc.BOLD,"Group: {group_name}, Name, Number of Symbols, Value, Invested, Return, Change, % of Total".format(group_name=group_name),bc.ENDC))

# Returns a header for CSV format data that aligns with to_string
def get_index_header(index_name):
    return ''.join((bc.BOLD,"{index_name}, Name, Number of Symbols, Value, Invested, Return, Change".format(index_name=index_name),bc.ENDC))

# Add a Share object to a category
# If the Share object is None, then create a new one
def add_share_to_category(name_str,share_obj,category_obj,primary=False,refresh=False):
    buyonly = BUYONLY == "True"
    if share_obj == None:
        share_obj = share.Share(name_str)
    if primary:
        share_obj.set_primary_category(category_obj.name)
    
    if refresh:
        share_obj.refresh()
        if PRINTSTATS:
            s = share_obj.get_stats_str(category_obj.name)
            if buyonly:
                if share_obj.buy_recommendation:
                    print(s)
                else:
                    pass
            else:
                print(s)

    category_obj.add_share(name_str,share_obj)

    return share_obj

# Parse a Json Category file and create Category + Share objects
def loadCategories(filename,primary_group_name="default"):
    groups_dict = dict()
    all_shares_dict = dict()
    with open(filename) as f:
        groups = f.read()
        if groups:
            d = json.loads(groups)
            for grp_name,cats in d.items():
                print("INFO: Loading group: %s" % grp_name)
                categories_dict = dict()
                group_obj = group.Group(grp_name,categories_dict)
                groups_dict[grp_name] = group_obj
                primary = grp_name == primary_group_name
                
                for cat_name,v in cats.items():
                    header = ''.join((grp_name," >> ",cat_name))

                    if PRINTSTATS:
                        print(share.get_stats_header(header))
                        
                    category_obj = category.Category(cat_name,primary)
                    if v.strip() != "*":
                        names = v.split(",")
                        for name in names:
                            n = name.strip()
                            if len(n) > 0: 
                                if n in all_shares_dict: # Add existing share objec
                                    add_share_to_category(name.strip(),all_shares_dict[n],category_obj,primary)
                                else: # Add new share object
                                    share_obj = add_share_to_category(name.strip(),None,category_obj,primary)
                                    all_shares_dict[name.strip()] = share_obj

                        if DEBUG:
                            print("DEBUG: ~~~~~~~~ Added %d shares for category: %s" % (category_obj.get_num_of_symbols(), category_obj.name))
                    else:
                        # This means the category says *
                        # TODO: How do we handle this condition
                        pass

                    categories_dict[cat_name] = category_obj
                    #print("%s%s%s" % (bc.OKGREEN, category_obj.get_buys_string(), bc.ENDC))

    return (groups_dict,all_shares_dict)

# Loads a Json Index file creates Category + Share objects
def loadIndex(filename):
    exclude = json.loads(EXCLUDESECTORS)
    buyonly = BUYONLY == "True"
    excluded = 0
    categories_dict = dict()
    shares_dict = dict()
    categories = None
    # Keep track of buys in an array
    buys = []
    with open(filename) as f:
        categories = f.read()
        if categories:
            d = json.loads(categories)
            if PRINTSTATS:
                print(share.get_stats_header("S&P500"))
            
            for item in d:
                
                # Process Sector - create a Category object per Sector
                sector_name = item["Sector"]
                if sector_name not in exclude:
                    category_obj = None
                    if sector_name not in categories_dict:
                        category_obj = category.Category(sector_name,False)
                        categories_dict[sector_name] = category_obj
                    else:
                        category_obj = categories_dict[sector_name]
                    
                    # Process Symbol
                    symbol = item["Symbol"]
                    share_obj = share.Share(symbol)
                    share_obj.set_primary_category(sector_name)
                    share_obj.refresh()
                    s = share_obj.get_stats_str("S&P500")
                    if share_obj.buy_recommendation:
                        buys.append(symbol)

                    if PRINTSTATS:
                        if buyonly:
                            if share_obj.buy_recommendation:
                                print(s)
                            else:
                                pass
                        else:
                            print(s)
                    
                    category_obj.add_share(symbol,share_obj)
                    shares_dict[symbol] = share_obj
                else:
                    excluded = excluded + 1
        
        print("%sProcessed %d sectors and %d symbols. Excluded: %d sectors and %d symbols%s" % (bc.BOLD,len(categories_dict),len(shares_dict),len(exclude),excluded,bc.ENDC))
        
        # Print buy reommendations
        print("%sBuy recommendations (%d): %s%s" % (bc.OKGREEN,len(buys),str(buys),bc.ENDC))
    
    return (categories_dict,shares_dict)

